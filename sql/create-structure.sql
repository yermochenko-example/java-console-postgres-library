CREATE TABLE "user" (
    "id"                  SERIAL   PRIMARY KEY,
    "login"               TEXT     NOT NULL UNIQUE,
    "password"            TEXT     NOT NULL,
    "role"                SMALLINT NOT NULL CHECK ("role" IN (1, 2, 3))
    /*
        1 - ADMIN
        2 - LIBRARIAN
        3 - READER
    */
);

CREATE TABLE "librarian" (
    "id"                  INTEGER  PRIMARY KEY REFERENCES "user"    ON UPDATE RESTRICT ON DELETE CASCADE, /* связь один-к-одному */
    "surname"             TEXT     NOT NULL,
    "name"                TEXT     NOT NULL
);

CREATE TABLE "reader" (
    "id"                  INTEGER  PRIMARY KEY REFERENCES "user"    ON UPDATE RESTRICT ON DELETE CASCADE, /* связь один-к-одному */
    "surname"             TEXT     NOT NULL,
    "name"                TEXT     NOT NULL,
    "address"             TEXT     NOT NULL
);

CREATE TABLE "phone" (
    "number"              TEXT     PRIMARY KEY, /* пример строкового первичного ключа - лучше так не делать */
    "reader_id"           INTEGER  NOT NULL REFERENCES "reader"     ON UPDATE RESTRICT ON DELETE CASCADE
);

CREATE TABLE "author" (
    "id"                  SERIAL   PRIMARY KEY,
    "surname"             TEXT     NOT NULL,
    "name"                TEXT     NOT NULL,
    "birth_year"          INTEGER  NOT NULL DEFAULT 1900,
    "death_year"          INTEGER
);

CREATE TABLE "cover_type" (
    "id"                  SERIAL   PRIMARY KEY,
    "name"                TEXT     NOT NULL
);

CREATE TABLE "book" (
    "id"                  SERIAL   PRIMARY KEY,
    "title"               TEXT     NOT NULL,
    "year"                INTEGER  NOT NULL,
    "cover_image"         BYTEA,
    "cover_type_id"       INTEGER  NOT NULL REFERENCES "cover_type" ON UPDATE RESTRICT ON DELETE RESTRICT
);

/* связь многие-ко-многим */
CREATE TABLE "author_vs_book" (
    "author_id"           INTEGER  NOT NULL REFERENCES "author"     ON UPDATE RESTRICT ON DELETE RESTRICT,
    "book_id"             INTEGER  NOT NULL REFERENCES "book"       ON UPDATE RESTRICT ON DELETE CASCADE,
    PRIMARY KEY("author_id", "book_id") /* пример составного первичного ключа - так тоже лучше не делать */
);

CREATE TABLE "book_copy" (
    "id"                  SERIAL   PRIMARY KEY,
    "book_id"             INTEGER  NOT NULL REFERENCES "book"       ON UPDATE RESTRICT ON DELETE RESTRICT,
    "notes"               TEXT     NOT NULL DEFAULT ''
);

CREATE TABLE "usage" (
    "id"                  SERIAL   PRIMARY KEY,
    "book_copy_id"        INTEGER  NOT NULL REFERENCES "book_copy"  ON UPDATE RESTRICT ON DELETE RESTRICT,
    "reader_id"           INTEGER  NOT NULL REFERENCES "reader"     ON UPDATE RESTRICT ON DELETE RESTRICT,
    "start_date"          DATE     NOT NULL,
    "start_librarian_id"  INTEGER  NOT NULL REFERENCES "librarian"  ON UPDATE RESTRICT ON DELETE RESTRICT,
    "plan_finish_date"    DATE     NOT NULL,
    "finish_date"         DATE,
    "finish_librarian_id" INTEGER           REFERENCES "librarian"  ON UPDATE RESTRICT ON DELETE RESTRICT /* необязательная связь */
);
