package by.vsu.jcpl.orm;

import by.vsu.jcpl.domain.BookCoverType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookCoverTypeDatabaseMapper extends EntityDatabaseMapper<BookCoverType> {
	public static final String SELECT_ALL_SQL = "SELECT \"id\", \"name\" FROM \"cover_type\"";
	public static final String SELECT_BY_ID_SQL = "SELECT \"id\", \"name\" FROM \"cover_type\" WHERE \"id\" = ?";
	public static final String INSERT_SQL = "INSERT INTO \"cover_type\" (\"name\") VALUES (?)";
	public static final String UPDATE_SQL = "UPDATE \"cover_type\" SET \"name\" = ? WHERE \"id\" = ?";
	public static final String DELETE_SQL = "DELETE FROM \"cover_type\" WHERE \"id\" = ?";

	public BookCoverTypeDatabaseMapper(Connection connection) {
		super(connection);
	}

	public List<BookCoverType> readAll() throws SQLException {
		List<BookCoverType> types = new ArrayList<>();
		readByCriteria(SELECT_ALL_SQL, null, types::add);
		return types;
	}

	@Override
	protected String readByIdQuery() {
		return SELECT_BY_ID_SQL;
	}

	@Override
	protected String createQuery() {
		return INSERT_SQL;
	}

	@Override
	protected String updateQuery() {
		return UPDATE_SQL;
	}

	@Override
	protected String deleteQuery() {
		return DELETE_SQL;
	}

	@Override
	protected void fillCreateStatement(PreparedStatement statement, BookCoverType type) throws SQLException {
		statement.setString(1, type.getName());
	}

	@Override
	protected void fillUpdateStatement(PreparedStatement statement, BookCoverType type) throws SQLException {
		fillCreateStatement(statement, type);
		statement.setInt(2, type.getId());
	}

	@Override
	protected BookCoverType parseResultSet(ResultSet resultSet) throws SQLException {
		BookCoverType type = new BookCoverType();
		type.setId(resultSet.getInt("id"));
		type.setName(resultSet.getString("name"));
		return type;
	}
}
