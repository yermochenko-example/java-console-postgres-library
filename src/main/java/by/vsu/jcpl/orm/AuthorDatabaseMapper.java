package by.vsu.jcpl.orm;

import by.vsu.jcpl.domain.Author;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AuthorDatabaseMapper extends EntityDatabaseMapper<Author> {
	public static final String SELECT_ALL_SQL = "SELECT \"id\", \"surname\", \"name\", \"birth_year\", \"death_year\" FROM \"author\"";
	public static final String SELECT_BY_ID_SQL = "SELECT \"id\", \"surname\", \"name\", \"birth_year\", \"death_year\" FROM \"author\" WHERE \"id\" = ?";
	public static final String INSERT_SQL = "INSERT INTO \"author\" (\"surname\", \"name\", \"birth_year\", \"death_year\") VALUES (?, ?, ?, ?)";
	public static final String UPDATE_SQL = "UPDATE \"author\" SET \"surname\" = ?, \"name\" = ?, \"birth_year\" = ?, \"death_year\" = ? WHERE \"id\" = ?";
	public static final String DELETE_SQL = "DELETE FROM \"author\" WHERE \"id\" = ?";

	public AuthorDatabaseMapper(Connection connection) {
		super(connection);
	}

	public List<Author> readAll() throws SQLException {
		List<Author> authors = new ArrayList<>();
		readByCriteria(SELECT_ALL_SQL, null, authors::add);
		return authors;
	}

	@Override
	protected String readByIdQuery() {
		return SELECT_BY_ID_SQL;
	}

	@Override
	protected String createQuery() {
		return INSERT_SQL;
	}

	@Override
	protected String updateQuery() {
		return UPDATE_SQL;
	}

	@Override
	protected String deleteQuery() {
		return DELETE_SQL;
	}

	@Override
	protected void fillCreateStatement(PreparedStatement statement, Author author) throws SQLException {
		statement.setString(1, author.getSurname());
		statement.setString(2, author.getName());
		statement.setInt(3, author.getBirthYear());
		if(author.getDeathYear() != null) {
			statement.setInt(4, author.getDeathYear());
		} else {
			statement.setNull(4, Types.INTEGER);
		}
	}

	@Override
	protected void fillUpdateStatement(PreparedStatement statement, Author author) throws SQLException {
		fillCreateStatement(statement, author);
		statement.setInt(5, author.getId());
	}

	@Override
	protected Author parseResultSet(ResultSet resultSet) throws SQLException {
		Author author = new Author();
		author.setId(resultSet.getInt("id"));
		author.setSurname(resultSet.getString("surname"));
		author.setName(resultSet.getString("name"));
		author.setBirthYear(resultSet.getInt("birth_year"));
		Integer deathYear = resultSet.getInt("death_year");
		if(!resultSet.wasNull()) {
			author.setDeathYear(deathYear);
		}
		return author;
	}
}
