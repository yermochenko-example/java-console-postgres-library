package by.vsu.jcpl.orm;

import by.vsu.jcpl.domain.Author;
import by.vsu.jcpl.domain.Book;
import by.vsu.jcpl.domain.BookCoverType;
import by.vsu.jcpl.domain.Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class BookDatabaseMapper extends EntityDatabaseMapper<Book> {
	public static final String SELECT_BY_AUTHOR_SQL = "SELECT \"book\".\"id\", \"book\".\"title\", \"book\".\"year\", \"book\".\"cover_type_id\" FROM \"book\" LEFT JOIN \"author_vs_book\" ON \"book\".\"id\" = \"author_vs_book\".\"book_id\" WHERE \"author_vs_book\".\"author_id\" = ?";
	public static final String SELECT_WITHOUT_AUTHORS_SQL = "SELECT \"book\".\"id\", \"book\".\"title\", \"book\".\"year\", \"book\".\"cover_type_id\" FROM \"book\" LEFT JOIN \"author_vs_book\" ON \"book\".\"id\" = \"author_vs_book\".\"book_id\" WHERE \"author_vs_book\".\"author_id\" IS NULL";
	public static final String SELECT_BY_ID_SQL = "SELECT \"id\", \"title\", \"year\", \"cover_type_id\" FROM \"book\" WHERE \"id\" = ?";
	public static final String INSERT_SQL = "INSERT INTO \"book\" (\"title\", \"year\", \"cover_type_id\") VALUES (?, ?, ?)";
	public static final String UPDATE_SQL = "UPDATE \"book\" SET \"title\" = ?, \"year\" = ?, \"cover_type_id\" = ? WHERE \"id\" = ?";
	public static final String DELETE_SQL = "DELETE FROM \"book\" WHERE \"id\" = ?";

	public static final String SELECT_AUTHORS_BY_BOOK_SQL = "SELECT \"author_id\" FROM \"author_vs_book\" WHERE \"book_id\" = ?";
	public static final String INSERT_AUTHORS_SQL = "INSERT INTO \"author_vs_book\" (\"author_id\", \"book_id\") VALUES (?, ?)";
	public static final String DELETE_AUTHORS_SQL = "DELETE FROM \"author_vs_book\" WHERE \"author_id\" = ? AND \"book_id\" = ?";

	private final AuthorDatabaseMapper authorDatabaseMapper;
	private final BookCoverTypeDatabaseMapper bookCoverTypeDatabaseMapper;

	public BookDatabaseMapper(Connection connection, AuthorDatabaseMapper authorDatabaseMapper, BookCoverTypeDatabaseMapper bookCoverTypeDatabaseMapper) {
		super(connection);
		this.authorDatabaseMapper = authorDatabaseMapper;
		this.bookCoverTypeDatabaseMapper = bookCoverTypeDatabaseMapper;
	}

	public List<Book> readByAuthor(Integer authorId) throws SQLException {
		List<Book> books = new ArrayList<>();
		Map<Integer, Author> authors = null;
		if(authorId != null) {
			readByCriteria(SELECT_BY_AUTHOR_SQL, statement -> statement.setInt(1, authorId), books::add);
			authors = readAuthors();
		} else {
			readByCriteria(SELECT_WITHOUT_AUTHORS_SQL, null, books::add);
		}
		Map<Integer, BookCoverType> coverTypes = readCoverTypes();
		for(Book book : books) {
			restoreReferences(book, coverTypes, authors);
		}
		return books;
	}

	@Override
	public Book readById(Integer id) throws SQLException {
		Book book = super.readById(id);
		restoreReferences(book, readCoverTypes(), readAuthors());
		return book;
	}

	@Override
	public Integer create(Book book) throws SQLException {
		Integer id = super.create(book);
		updateAuthorIds(id, book.getAuthors().stream().map(Entity::getId).collect(Collectors.toSet()), INSERT_AUTHORS_SQL);
		return id;
	}

	@Override
	public void update(Book book) throws SQLException {
		super.update(book);
		Set<Integer> oldAuthorIds = readAuthorIdsByBook(book.getId());
		Set<Integer> newAuthorIds = book.getAuthors().stream().map(Entity::getId).collect(Collectors.toSet());
		if(!oldAuthorIds.equals(newAuthorIds)) {
			Set<Integer> nonChangedAuthorIds = new LinkedHashSet<>(oldAuthorIds);
			nonChangedAuthorIds.retainAll(newAuthorIds);
			Set<Integer> removingAuthorIds = new LinkedHashSet<>(oldAuthorIds);
			removingAuthorIds.removeAll(nonChangedAuthorIds);
			Set<Integer> addingAuthorIds = new LinkedHashSet<>(newAuthorIds);
			addingAuthorIds.removeAll(nonChangedAuthorIds);
			updateAuthorIds(book.getId(), removingAuthorIds, DELETE_AUTHORS_SQL);
			updateAuthorIds(book.getId(), addingAuthorIds, INSERT_AUTHORS_SQL);
		}
	}

	@Override
	protected String readByIdQuery() {
		return SELECT_BY_ID_SQL;
	}

	@Override
	protected String createQuery() {
		return INSERT_SQL;
	}

	@Override
	protected String updateQuery() {
		return UPDATE_SQL;
	}

	@Override
	protected String deleteQuery() {
		return DELETE_SQL;
	}

	@Override
	protected void fillCreateStatement(PreparedStatement statement, Book book) throws SQLException {
		statement.setString(1, book.getTitle());
		statement.setInt(2, book.getYear());
		statement.setInt(3, book.getCoverType().getId());
	}

	@Override
	protected void fillUpdateStatement(PreparedStatement statement, Book book) throws SQLException {
		fillCreateStatement(statement, book);
		statement.setInt(4, book.getId());
	}

	@Override
	protected Book parseResultSet(ResultSet resultSet) throws SQLException {
		Book book = new Book();
		book.setId(resultSet.getInt("id"));
		book.setTitle(resultSet.getString("title"));
		book.setYear(resultSet.getInt("year"));
		book.setCoverType(new BookCoverType());
		book.getCoverType().setId(resultSet.getInt("cover_type_id"));
		return book;
	}

	private void restoreReferences(Book book, Map<Integer, BookCoverType> coverTypes, Map<Integer, Author> authors) throws SQLException {
		book.setCoverType(coverTypes.get(book.getCoverType().getId()));
		if(authors != null) {
			Set<Integer> authorIds = readAuthorIdsByBook(book.getId());
			for(Integer id : authorIds) {
				book.getAuthors().add(authors.get(id));
			}
		}
	}

	private Set<Integer> readAuthorIdsByBook(Integer bookId) throws SQLException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = getConnection().prepareStatement(SELECT_AUTHORS_BY_BOOK_SQL);
			statement.setInt(1, bookId);
			resultSet = statement.executeQuery();
			Set<Integer> authorIds = new LinkedHashSet<>();
			while(resultSet.next()) {
				authorIds.add(resultSet.getInt("author_id"));
			}
			return authorIds;
		} finally {
			try { Objects.requireNonNull(resultSet).close(); } catch(Exception ignored) {}
			try { Objects.requireNonNull(statement).close(); } catch(Exception ignored) {}
		}
	}

	private void updateAuthorIds(Integer bookId, Set<Integer> authorIds, String sql) throws SQLException {
		if(!authorIds.isEmpty()) {
			PreparedStatement statement = null;
			try {
				statement = getConnection().prepareStatement(sql);
				for(Integer authorId : authorIds) {
					statement.setInt(1, authorId);
					statement.setInt(2, bookId);
					statement.executeUpdate();
				}
			} finally {
				try { Objects.requireNonNull(statement).close(); } catch(Exception ignored) {}
			}
		}
	}

	private Map<Integer, Author> readAuthors() throws SQLException {
		Map<Integer, Author> authors = new HashMap<>();
		for(Author author : authorDatabaseMapper.readAll()) {
			authors.put(author.getId(), author);
		}
		return authors;
	}

	private Map<Integer, BookCoverType> readCoverTypes() throws SQLException {
		Map<Integer, BookCoverType> coverTypes = new HashMap<>();
		for(BookCoverType coverType : bookCoverTypeDatabaseMapper.readAll()) {
			coverTypes.put(coverType.getId(), coverType);
		}
		return coverTypes;
	}
}
