package by.vsu.jcpl;

import by.vsu.jcpl.menu.*;
import by.vsu.jcpl.orm.AuthorDatabaseMapper;
import by.vsu.jcpl.orm.BookCoverTypeDatabaseMapper;
import by.vsu.jcpl.orm.BookDatabaseMapper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

public class Main {
	public static void main(String[] args) {
		Connection connection = null;
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/library_db", "root", "root");
			System.out.println("Подключение к базе данных успешно выполнено");
			AuthorDatabaseMapper authorDatabaseMapper = new AuthorDatabaseMapper(connection);
			BookCoverTypeDatabaseMapper bookCoverTypeDatabaseMapper = new BookCoverTypeDatabaseMapper(connection);
			BookDatabaseMapper bookDatabaseMapper = new BookDatabaseMapper(connection, authorDatabaseMapper, bookCoverTypeDatabaseMapper);
			List<MenuItem> menuItems = new ArrayList<>();
			menuItems.add(new BookListMenuItem("Просмотр списка книг указанного автора", bookDatabaseMapper));
			menuItems.add(new BookAddMenuItem("Добавление новой книги", bookDatabaseMapper));
			menuItems.add(new BookEditMenuItem("Изменение данных книги", bookDatabaseMapper));
			menuItems.add(new BookDeleteMenuItem("Удаление данных книги", bookDatabaseMapper));
			menuItems.add(new AuthorListMenuItem("Просмотр списка авторов", authorDatabaseMapper));
			menuItems.add(new AuthorAddMenuItem("Добавление нового автора", authorDatabaseMapper));
			menuItems.add(new AuthorEditMenuItem("Изменение данных автора", authorDatabaseMapper));
			menuItems.add(new AuthorDeleteMenuItem("Удаление данных автора", authorDatabaseMapper));
			menuItems.add(new BookCoverTypeListMenuItem("Просмотр списка типов обложек", bookCoverTypeDatabaseMapper));
			menuItems.add(new BookCoverTypeAddMenuItem("Добавление нового типа обложки", bookCoverTypeDatabaseMapper));
			menuItems.add(new BookCoverTypeEditMenuItem("Изменение данных о типе обложки", bookCoverTypeDatabaseMapper));
			menuItems.add(new BookCoverTypeDeleteMenuItem("Удаление данных о типе обложки", bookCoverTypeDatabaseMapper));
			menuItems.add(new ExitMenuItem("Выход"));
			int width = menuItems.stream().map(item -> item.title().length()).max(Integer::compareTo).get();
			String delimiter = "+" + String.join("", Collections.nCopies(width + 6, "-")) + "+";
			Scanner console = new Scanner(System.in);
			boolean work = true;
			while(work) {
				System.out.println(delimiter);
				System.out.printf("| %-" + (width + 5) + "s|\n", "МЕНЮ:");
				System.out.println(delimiter);
				int n = 1;
				for(MenuItem menuItem : menuItems) {
					System.out.printf("| %2d) %-" + width + "s |\n", n++, menuItem.title());
				}
				System.out.println(delimiter);
				System.out.print("\nВаш выбор: ");
				try {
					work = menuItems.get(Integer.parseInt(console.nextLine()) - 1).activate();
					System.out.println("\n******************************\n");
				} catch(IndexOutOfBoundsException | NumberFormatException e) {
					System.out.println("Некорректный пункт меню");
				} catch(EntityValidationException e) {
					System.out.println(e.getMessage());
				} catch(SQLException e) {
					System.out.println("Ошибка работы с базой данных");
					e.printStackTrace();
				}
			}
		} catch(ClassNotFoundException e) {
			System.out.println("Не удалось загрузить драйвер базы данных");
			e.printStackTrace();
		} catch(SQLException e) {
			System.out.println("Не удалось подключиться к базе данных");
			e.printStackTrace();
		} finally {
			try { Objects.requireNonNull(connection).close(); } catch(Exception ignored) {}
		}
	}
}
