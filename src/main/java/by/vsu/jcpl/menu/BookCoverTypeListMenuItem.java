package by.vsu.jcpl.menu;

import by.vsu.jcpl.domain.BookCoverType;
import by.vsu.jcpl.orm.BookCoverTypeDatabaseMapper;

import java.sql.SQLException;
import java.util.List;

public class BookCoverTypeListMenuItem extends BookCoverTypeMenuItem {
	public BookCoverTypeListMenuItem(String title, BookCoverTypeDatabaseMapper bookCoverTypeDatabaseMapper) {
		super(title, bookCoverTypeDatabaseMapper);
	}

	public boolean activate() throws SQLException {
		System.out.println("\n==<[ ТИПЫ ОБЛОЖЕК ]>==\n");
		List<BookCoverType> types = getBookCoverTypeDatabaseMapper().readAll();
		for(BookCoverType type : types) {
			System.out.printf("[%04d] %s\n", type.getId(), type.getName());
		}
		return true;
	}
}
