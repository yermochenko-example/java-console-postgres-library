package by.vsu.jcpl.menu;

import by.vsu.jcpl.EntityValidationException;
import by.vsu.jcpl.domain.BookCoverType;
import by.vsu.jcpl.orm.BookCoverTypeDatabaseMapper;

import java.sql.SQLException;

public class BookCoverTypeAddMenuItem extends BookCoverTypeMenuItem {
	public BookCoverTypeAddMenuItem(String title, BookCoverTypeDatabaseMapper bookCoverTypeDatabaseMapper) {
		super(title, bookCoverTypeDatabaseMapper);
	}

	public boolean activate() throws SQLException, EntityValidationException {
		System.out.println("\n==<[ ДОБАВЛЕНИЕ НОВОГО ТИПА ОБЛОЖКИ КНИГ ]>==\n");
		BookCoverType type = new BookCoverType();
		System.out.print("Введите название типа обложки: ");
		String name = console.nextLine();
		if(!name.isBlank()) {
			type.setName(name);
		} else {
			throw new EntityValidationException("Название не должно быть пустым");
		}
		Integer id = getBookCoverTypeDatabaseMapper().create(type);
		System.out.printf("Тип обложки успешно добавлен с идентификатором [%04d]\n", id);
		return true;
	}
}
