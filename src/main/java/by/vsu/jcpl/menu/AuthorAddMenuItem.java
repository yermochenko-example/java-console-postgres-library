package by.vsu.jcpl.menu;

import by.vsu.jcpl.domain.Author;
import by.vsu.jcpl.orm.AuthorDatabaseMapper;
import by.vsu.jcpl.EntityValidationException;

import java.sql.SQLException;

public class AuthorAddMenuItem extends AuthorMenuItem {
	public AuthorAddMenuItem(String title, AuthorDatabaseMapper authorDatabaseMapper) {
		super(title, authorDatabaseMapper);
	}

	public boolean activate() throws SQLException, EntityValidationException {
		System.out.println("\n==<[ ДОБАВЛЕНИЕ НОВОГО АВТОРА ]>==\n");
		Author author = new Author();
		System.out.print("Введите фамилию автора: ");
		String surname = console.nextLine();
		if(!surname.isBlank()) {
			author.setSurname(surname);
		} else {
			throw new EntityValidationException("Фамилия не должна быть пустой");
		}
		System.out.print("Введите имя автора: ");
		String name = console.nextLine();
		if(!name.isBlank()) {
			author.setName(name);
		} else {
			throw new EntityValidationException("Имя не должно быть пустым");
		}
		System.out.print("Введите год рождения автора: ");
		String birthYear = console.nextLine();
		try {
			author.setBirthYear(Integer.valueOf(birthYear));
		} catch(NumberFormatException e) {
			throw new EntityValidationException("Год рождения не должен быть пустым и должен быть целым числом");
		}
		System.out.print("Введите год смерти автора (или нажмите ввод, если автор жив): ");
		String deathYear = console.nextLine();
		if(!deathYear.isBlank()) {
			try {
				author.setDeathYear(Integer.valueOf(deathYear));
			} catch(NumberFormatException e) {
				throw new EntityValidationException("Год смерти должен быть целым числом");
			}
		}
		if(author.getDeathYear() != null && author.getBirthYear() > author.getDeathYear()) {
			throw new EntityValidationException("Год рождения должен быть меньше года смерти");
		}
		Integer id = getAuthorDatabaseMapper().create(author);
		System.out.printf("Автор успешно добавлен с идентификатором [%04d]\n", id);
		return true;
	}
}
