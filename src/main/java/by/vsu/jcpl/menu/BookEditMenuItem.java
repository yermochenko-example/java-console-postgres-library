package by.vsu.jcpl.menu;

import by.vsu.jcpl.EntityValidationException;
import by.vsu.jcpl.domain.Author;
import by.vsu.jcpl.domain.Book;
import by.vsu.jcpl.domain.BookCoverType;
import by.vsu.jcpl.orm.BookDatabaseMapper;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Set;

public class BookEditMenuItem extends BookMenuItem {
	public BookEditMenuItem(String title, BookDatabaseMapper bookDatabaseMapper) {
		super(title, bookDatabaseMapper);
	}

	public boolean activate() throws SQLException, EntityValidationException {
		System.out.println("\n==<[ ИЗМЕНЕНИЕ ДАННЫХ КНИГИ ]>==\n");
		System.out.print("Введите идентификатор книги: ");
		int id;
		try {
			id = Integer.parseInt(console.nextLine());
		} catch(NumberFormatException e) {
			throw new EntityValidationException("Идентификатор книги должен быть целым числом");
		}
		Book book = getBookDatabaseMapper().readById(id);
		if(book != null) {
			Set<Author> authors = book.getAuthors();
			if(authors.isEmpty()) {
				System.out.println("У книги нет автора");
			} else {
				if(authors.size() == 1) {
					Author author = authors.iterator().next();
					System.out.printf("Автор книги: %s %s (%d)\n", author.getName(), author.getSurname(), author.getId());
				} else {
					System.out.println("Авторы книги:");
					for(Author author : authors) {
						System.out.printf("\t%s %s (%d)\n", author.getName(), author.getSurname(), author.getId());
					}
				}
			}
			System.out.print("Введите идентификатор нового автора (или через запятую идентификаторы новых авторов, или введите символ \"-\", если авторов нет, или нажмите ввод, если автора(ов) изменять не нужно): ");
			String authorIds = console.nextLine();
			if(!authorIds.isBlank()) {
				authors.clear();
				if(!"-".equals(authorIds)) {
					try {
						Arrays.stream(authorIds.split(",")).forEach(authorId -> {
							Author author = new Author();
							author.setId(Integer.valueOf(authorId.trim()));
							authors.add(author);
						});
					} catch(NumberFormatException e) {
						throw new EntityValidationException("Идентификаторы авторов должны быть целыми числами");
					}
				}
			}
			System.out.printf("Заглавие книги: %s\n", book.getTitle());
			System.out.print("Введите новое заглавие книги (или нажмите ввод, если заглавие не нужно менять): ");
			String title = console.nextLine();
			if(!title.isBlank()) {
				book.setTitle(title);
			}
			System.out.printf("Год издания книги: %d\n", book.getYear());
			System.out.print("Введите новый год издания книги (или нажмите ввод, если год издания изменять не нужно): ");
			String year = console.nextLine();
			if(!year.isBlank()) {
				try {
					book.setYear(Integer.valueOf(year));
				} catch(NumberFormatException e) {
					throw new EntityValidationException("Год издания книги должен быть целым числом");
				}
			}
			System.out.printf("Тип обложки: %s (%d)\n", book.getCoverType().getName(), book.getCoverType().getId());
			System.out.print("Введите идентификатор нового типа обложки книги (или нажмите ввод, если тип обложки изменять не нужно): ");
			String typeId = console.nextLine();
			if(!typeId.isBlank()) {
				try {
					book.setCoverType(new BookCoverType());
					book.getCoverType().setId(Integer.valueOf(typeId));
				} catch(NumberFormatException e) {
					throw new EntityValidationException("Идентификатор типа обложки книги должен быть целым числом");
				}
			}
			getBookDatabaseMapper().update(book);
			System.out.println("Данные книги успешно обновлены");
		} else {
			System.out.println("Книги с таким идентификатором не найдено");
		}
		return true;
	}
}
