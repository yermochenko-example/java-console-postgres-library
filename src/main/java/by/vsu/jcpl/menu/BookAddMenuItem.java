package by.vsu.jcpl.menu;

import by.vsu.jcpl.EntityValidationException;
import by.vsu.jcpl.domain.Author;
import by.vsu.jcpl.domain.Book;
import by.vsu.jcpl.domain.BookCoverType;
import by.vsu.jcpl.orm.BookDatabaseMapper;

import java.sql.SQLException;
import java.util.Arrays;

public class BookAddMenuItem extends BookMenuItem {
	public BookAddMenuItem(String title, BookDatabaseMapper bookDatabaseMapper) {
		super(title, bookDatabaseMapper);
	}

	public boolean activate() throws SQLException, EntityValidationException {
		System.out.println("\n==<[ ДОБАВЛЕНИЕ НОВОЙ КНИГИ ]>==\n");
		Book book = new Book();
		System.out.print("Введите идентификатор автора (или через запятую идентификаторы авторов, или нажмите ввод, если авторов нет): ");
		String authorIds = console.nextLine();
		if(!authorIds.isBlank()) {
			try {
				Arrays.stream(authorIds.split(",")).forEach(id -> {
					Author author = new Author();
					author.setId(Integer.valueOf(id.trim()));
					book.getAuthors().add(author);
				});
			} catch(NumberFormatException e) {
				throw new EntityValidationException("Идентификаторы авторов должны быть целыми числами");
			}
		}
		System.out.print("Введите заглавие книги: ");
		String title = console.nextLine();
		if(!title.isBlank()) {
			book.setTitle(title);
		} else {
			throw new EntityValidationException("Заглавие книги не должно быть пустым");
		}
		System.out.print("Введите год издания книги: ");
		String year = console.nextLine();
		try {
			book.setYear(Integer.valueOf(year));
		} catch(NumberFormatException e) {
			throw new EntityValidationException("Год издания книги не должен быть пустым и должен быть целым числом");
		}
		System.out.print("Введите идентификатор типа обложки книги: ");
		String typeId = console.nextLine();
		try {
			book.setCoverType(new BookCoverType());
			book.getCoverType().setId(Integer.valueOf(typeId));
		} catch(NumberFormatException e) {
			throw new EntityValidationException("Идентификатор типа обложки книги не должен быть пустым и должен быть целым числом");
		}
		Integer id = getBookDatabaseMapper().create(book);
		System.out.printf("Книга успешно добавлена с идентификатором [%04d]\n", id);
		return true;
	}
}
