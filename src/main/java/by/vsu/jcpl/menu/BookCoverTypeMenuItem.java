package by.vsu.jcpl.menu;

import by.vsu.jcpl.orm.BookCoverTypeDatabaseMapper;

abstract public class BookCoverTypeMenuItem extends NamedMenuItem {
	private final BookCoverTypeDatabaseMapper bookCoverTypeDatabaseMapper;

	protected BookCoverTypeMenuItem(String title, BookCoverTypeDatabaseMapper bookCoverTypeDatabaseMapper) {
		super(title);
		this.bookCoverTypeDatabaseMapper = bookCoverTypeDatabaseMapper;
	}

	protected BookCoverTypeDatabaseMapper getBookCoverTypeDatabaseMapper() {
		return bookCoverTypeDatabaseMapper;
	}
}
