package by.vsu.jcpl.menu;

import by.vsu.jcpl.EntityValidationException;
import by.vsu.jcpl.orm.AuthorDatabaseMapper;

import java.sql.SQLException;

public class AuthorDeleteMenuItem extends AuthorMenuItem {
	public AuthorDeleteMenuItem(String title, AuthorDatabaseMapper authorDatabaseMapper) {
		super(title, authorDatabaseMapper);
	}

	public boolean activate() throws SQLException, EntityValidationException {
		System.out.println("\n==<[ УДАЛЕНИЕ ДАННЫХ АВТОРА ]>==\n");
		System.out.print("Введите идентификатор автора: ");
		int id;
		try {
			id = Integer.parseInt(console.nextLine());
		} catch(NumberFormatException e) {
			throw new EntityValidationException("Идентификатор автора должен быть целым числом");
		}
		getAuthorDatabaseMapper().delete(id);
		System.out.println("Данные автора успешно удалены");
		return true;
	}
}
