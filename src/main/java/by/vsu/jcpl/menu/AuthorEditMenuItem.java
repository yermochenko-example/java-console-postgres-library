package by.vsu.jcpl.menu;

import by.vsu.jcpl.EntityValidationException;
import by.vsu.jcpl.domain.Author;
import by.vsu.jcpl.orm.AuthorDatabaseMapper;

import java.sql.SQLException;

public class AuthorEditMenuItem extends AuthorMenuItem {
	public AuthorEditMenuItem(String title, AuthorDatabaseMapper authorDatabaseMapper) {
		super(title, authorDatabaseMapper);
	}

	public boolean activate() throws SQLException, EntityValidationException {
		System.out.println("\n==<[ ИЗМЕНЕНИЕ ДАННЫХ АВТОРА ]>==\n");
		System.out.print("Введите идентификатор автора: ");
		int id;
		try {
			id = Integer.parseInt(console.nextLine());
		} catch(NumberFormatException e) {
			throw new EntityValidationException("Идентификатор автора должен быть целым числом");
		}
		Author author = getAuthorDatabaseMapper().readById(id);
		if(author != null) {
			System.out.printf("Фамилия автора: %s\n", author.getSurname());
			System.out.print("Введите новую фамилию автора (или нажмите ввод, если фамилию не нужно менять): ");
			String surname = console.nextLine();
			if(!surname.isBlank()) {
				author.setSurname(surname);
			}
			System.out.printf("Имя автора: %s\n", author.getName());
			System.out.print("Введите новое имя автора (или нажмите ввод, если имя не нужно менять): ");
			String name = console.nextLine();
			if(!name.isBlank()) {
				author.setName(name);
			}
			System.out.printf("Год рождения автора: %d\n", author.getBirthYear());
			System.out.print("Введите новый год рождения автора (или нажмите ввод, если год рождения не нужно менять): ");
			String birthYear = console.nextLine();
			if(!birthYear.isBlank()) {
				try {
					author.setBirthYear(Integer.valueOf(birthYear));
				} catch(NumberFormatException e) {
					throw new EntityValidationException("Год рождения должен быть целым числом");
				}
			}
			System.out.print("Год смерти автора: ");
			if(author.getDeathYear() != null) {
				System.out.printf("%d\n", author.getDeathYear());
			} else {
				System.out.println("---");
			}
			System.out.print("Введите новый год смерти автора (или нажмите ввод, если год смерти не нужно менять, или введите \"-\" если год смерти нужно очистить): ");
			String deathYear = console.nextLine();
			if(!deathYear.isBlank()) {
				if("-".equals(deathYear)) {
					author.setDeathYear(null);
				} else {
					try {
						author.setDeathYear(Integer.valueOf(deathYear));
					} catch(NumberFormatException e) {
						throw new EntityValidationException("Год смерти должен быть целым числом");
					}
				}
			}
			if(author.getDeathYear() != null && author.getBirthYear() > author.getDeathYear()) {
				throw new EntityValidationException("Год рождения должен быть меньше года смерти");
			}
			getAuthorDatabaseMapper().update(author);
			System.out.println("Данные автора успешно обновлены");
		} else {
			System.out.println("Автора с таким идентификатором не найдено");
		}
		return true;
	}
}
