package by.vsu.jcpl.menu;

import by.vsu.jcpl.EntityValidationException;
import by.vsu.jcpl.domain.BookCoverType;
import by.vsu.jcpl.orm.BookCoverTypeDatabaseMapper;

import java.sql.SQLException;

public class BookCoverTypeEditMenuItem extends BookCoverTypeMenuItem {
	public BookCoverTypeEditMenuItem(String title, BookCoverTypeDatabaseMapper bookCoverTypeDatabaseMapper) {
		super(title, bookCoverTypeDatabaseMapper);
	}

	public boolean activate() throws SQLException, EntityValidationException {
		System.out.println("\n==<[ ИЗМЕНЕНИЕ ДАННЫХ О ТИПЕ ОБЛОЖКИ ]>==\n");
		System.out.print("Введите идентификатор типа обложки: ");
		int id;
		try {
			id = Integer.parseInt(console.nextLine());
		} catch(NumberFormatException e) {
			throw new EntityValidationException("Идентификатор типа обложки должен быть целым числом");
		}
		BookCoverType type = getBookCoverTypeDatabaseMapper().readById(id);
		if(type != null) {
			System.out.printf("Название типа обложки: %s\n", type.getName());
			System.out.print("Введите новое название типа обложки (или нажмите ввод, если название не нужно менять): ");
			String name = console.nextLine();
			if(!name.isBlank()) {
				type.setName(name);
			}
			getBookCoverTypeDatabaseMapper().update(type);
			System.out.println("Данные о типе обложки успешно обновлены");
		} else {
			System.out.println("Типа обложки с таким идентификатором не существует");
		}
		return true;
	}
}
