package by.vsu.jcpl.menu;

import by.vsu.jcpl.EntityValidationException;
import by.vsu.jcpl.orm.BookDatabaseMapper;

import java.sql.SQLException;

public class BookDeleteMenuItem extends BookMenuItem {
	public BookDeleteMenuItem(String title, BookDatabaseMapper bookDatabaseMapper) {
		super(title, bookDatabaseMapper);
	}

	public boolean activate() throws SQLException, EntityValidationException {
		System.out.println("\n==<[ УДАЛЕНИЕ ДАННЫХ КНИГИ ]>==\n");
		System.out.print("Введите идентификатор книги: ");
		int id;
		try {
			id = Integer.parseInt(console.nextLine());
		} catch(NumberFormatException e) {
			throw new EntityValidationException("Идентификатор книги должен быть целым числом");
		}
		getBookDatabaseMapper().delete(id);
		System.out.println("Данные книги успешно удалены");
		return true;
	}
}
