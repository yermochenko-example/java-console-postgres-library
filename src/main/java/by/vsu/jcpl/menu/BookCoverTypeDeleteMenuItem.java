package by.vsu.jcpl.menu;

import by.vsu.jcpl.EntityValidationException;
import by.vsu.jcpl.orm.BookCoverTypeDatabaseMapper;

import java.sql.SQLException;

public class BookCoverTypeDeleteMenuItem extends BookCoverTypeMenuItem {
	public BookCoverTypeDeleteMenuItem(String title, BookCoverTypeDatabaseMapper bookCoverTypeDatabaseMapper) {
		super(title, bookCoverTypeDatabaseMapper);
	}

	public boolean activate() throws SQLException, EntityValidationException {
		System.out.println("\n==<[ УДАЛЕНИЕ ДАННЫХ О ТИПЕ ОБЛОЖКИ ]>==\n");
		System.out.print("Введите идентификатор типа обложки: ");
		int id;
		try {
			id = Integer.parseInt(console.nextLine());
		} catch(NumberFormatException e) {
			throw new EntityValidationException("Идентификатор типа обложки должен быть целым числом");
		}
		getBookCoverTypeDatabaseMapper().delete(id);
		System.out.println("Данные о типе обложки успешно удалены");
		return true;
	}
}
